(define (make-monitored f)
  (define acc 0)
  
  (lambda (args)
    (cond
      ((eq? 'how-many-calls? args) acc)
      ((eq? args 'reset-count) (begin (set! acc 0)))
      (else (begin (set! acc (+ acc 1))
                   (apply f args)))
      )))  
    

(define s (make-monitored sqrt))
  
(display (s '(100))) (newline)

(display (s 'how-many-calls?)) (newline)

(display (s '(100))) (newline)

(display (s 'how-many-calls?)) (newline)

(s 'reset-count)

(display (s 'how-many-calls?)) (newline)

