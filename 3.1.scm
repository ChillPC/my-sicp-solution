(define (make-accumulator value)
  (lambda (n)
    (begin
      (set! value (+ value n))
      value)))
    
(define A (make-accumulator 5))

(display (A 10)) (newline)
(display (A 10)) (newline)

(define B (make-accumulator 5))

(display (B 10)) (newline)
(display (B 10)) (newline)
