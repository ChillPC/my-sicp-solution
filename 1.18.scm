
;; Exercise 1.18: Using the results of Exercise 1.16 and Exercise 1.17, devise a procedure that generates an iterative process for multiplying two integers in terms of adding, doubling, and halving and uses a logarithmic number of steps.40 


(define (fast-mult-iter a b)
  (rec (a a) (b b) (m m))
  (cond
    [(= b 0) m]
    [(even? b) (fast-mult-iter m (double a) (halve b))]
    [else (fast-mult-iter (+ m a) a (- b 1))]))
