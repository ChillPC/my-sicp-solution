;; Exercise 1.11.  A function f is defined by the rule that f(n) = n if n<3 and f(n) = f(n - 1) + 2f(n - 2) + 3f(n - 3) if n>=3. Write a procedure that computes f by means of a recursive process. Write a procedure that computes f by means of an iterative process. 

(define (f-rec n)
  (cond
    [(< n 3) n]
    [else
      (+ (f (- n 1))
         (* 2 (f (- n 2)))
         (* 3 (f (- n 3))))]))

;; (display "(f-rec 10) : ") (display (f-rec 10)) (newline)

(define (f-inc n)
  (let f-inc-acc [(n n) (r 2) (r1 1) (r2 0)]
    (if (< n 3)
      r
      (f-inc-acc (- n 1) (+ r (* 2 r1) (* 3 r2)) r r1))))

