(define (make-account balance password)
  (define max-remaining-tries 3)

  (define remaining-tries max-remaining-tries)

  (define (withdraw amount)
    (if (>= balance amount)
      (begin (set! balance
               (- balance amount))
             balance)
      "Insufficient funds"))

  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)

  (define (dispatch given-password m)
    (if (eq? password given-password)
      (begin (set! remaining-tries max-remaining-tries)
             (cond ((eq? m 'withdraw) withdraw)
                   ((eq? m 'deposit) deposit)
                   (else (error "Unknown request: MAKE-ACCOUNT" m))))

      (begin (set! remaining-tries (- remaining-tries 1))
             (if (<= remaining-tries 0)
               (lambda (args) (call-the-cops))
               (lambda (args) (display "Incorrect pasword"))))))

  dispatch)

(define (call-the-cops)
  (display "Cops called!!!!!"))

(define acc
  (make-account 100 'secret-password))

((acc 'some-other-password 'deposit) 50) (newline)

((acc 'secret-password 'withdraw) 10) (newline)

((acc 'some-other-password 'deposit) 50) (newline)

((acc 'some-other-password 'deposit) 50) (newline)

((acc 'some-other-password 'deposit) 50) (newline)

((acc 'some-other-password 'deposit) 50) (newline)


